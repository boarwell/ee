export const create = <Key, Arg, Ret>() => {
  const map = new Map<Key, Array<(arg: Arg) => Ret>>();

  return {
    on(k: Key, f: (arg: Arg) => Ret): void {
      map.set(k, [...(map.get(k) || []), f]);
    },

    off(k: Key): void {
      map.delete(k);
    },

    emit(k: Key, arg: Arg): Ret[] {
      return (map.get(k) || []).map(f => f.call(null, arg));
    }
  };
};
